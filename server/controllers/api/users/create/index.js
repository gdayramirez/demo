const users = require('../../../../models/users')


exports.init = async (ctx)=>{
    let { body } = ctx.request
    body.createdAt = new Date().toISOString()
    const userOne = users.create(body)
    const [taskOne] = await Promise.all([userOne])
    if (!taskOne) {
        ctx.status = 400
        ctx.body = { label : 'DB ERROR' }
    } else {
        ctx.status = 201
        ctx.body = {label: 'created', user: taskOne}
    }
}
