const users = require('../../../../models/users')


exports.init = async (ctx)=>{
    const userOne = users.updateOne({_id :ctx.params._id},{$set:{
        status : 'removed',
        removedAt : new Date().toISOString()
    }})
    const [taskOne] = await Promise.all([userOne])
    if (!taskOne) {
        ctx.status = 400
        ctx.body = { label : 'DB ERROR' }
    } else {
        ctx.status = 200
        ctx.body = {label: 'deleted', _id: ctx.params._id}
    }
}
