const users = require('../../../../models/users')


exports.init = async (ctx)=>{
    const userOne = users.find({})
    const [taskOne] = await Promise.all([userOne])
    if (!taskOne) {
        ctx.status = 400
        ctx.body = { label : 'DB ERROR' }
    } else {
        ctx.status = 200
        ctx.body = {users: taskOne}
    }
}


exports.getOne = async (ctx)=>{
    const userOne = users.find({_id:ctx.params._id})
    const [taskOne] = await Promise.all([userOne])
    if (!taskOne) {
        ctx.status = 400
        ctx.body = { label : 'DB ERROR' }
    } else {
        ctx.status = 200
        ctx.body = {user: taskOne[0]}
    }
}