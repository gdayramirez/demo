exports.init = (ctx, next) => {
    let { body } = ctx.request
    if (!this.isValid(body)) {
        ctx.status = 400
        ctx.body = { label: 'Array is not valid' }
    }
    next()
}

exports.getResults = (ctx, next) => {
    let { body } = ctx.request
    let word = this.longest(body)
    ctx.status = 200
    ctx.body = {
        word : word,
        array : body,
        index : body.findIndex(e => e === word)
    }
}
/**
 * @param {array} 
 * @description Recibe como parametro un array y devuelve el con la longitud mas alta encontrada
 */
exports.longest = (array) => {
    return Array.from(array).sort((a, b) => b.length - a.length)[0];
}
/**
 * @param {array} 
 * @description Recibe como parametro un array y si el array es valido a las siguientes reglas :
 * -> Es tipo array
 * -> Es mayor a 2 elementos
 * -> Es menor a 8 elementos
 */
exports.isValid = (array) => Array.isArray(array) && array.length >= 2 && array.length <= 8 ? true : false;


